# exemple commandes htrdr-multi
# NV 21/02/23

############
# les2htcp #
############
# préciser avec -l la liste des variables nuageuses à ajouter : RCT, RIT, RRT, RST
# si on ajoute RCT, il faut qu'il y ait un champ "liquid_effective_radius"
# si on ajoute RIT, il faut qu'il y ait un champ "ice_effective_radius"
# si on ajoute RRT, il faut qu'il y ait un champ "rain_effective_radius"
# si on ajoute RST, il faut qu'il y ait un champ "snow_effective_radius"
# champs obligatoires : RVT, THT, PABST, W_E_direction, S_N_direction, vertical_levels

# exemple 1, que l'eau liquide, convertit les coordonnées de km en m
les2htcp -l RCT -m 1000 -i LES.nc -o nuages_eau_liquide.htcp

# exemple 2, eau liquide et pluie, convertit les coordonnées de km en m
les2htcp -l RCT,RRT -m 1000 -i LES.nc -o nuages_eau_liquide_et_pluie.htcp

############
#  htrdr   #
############
# préciser le nom de la variable associée au fichier de Mie dans l'option -m
# avant : -m Mie_LUT_Cloud.nc ; maintenant : -m name=RCT:radprop=Mie_LUT_Cloud.nc
# autant de -m que de composantes à prendre en compte dans le calcul radiatif
# on peut utiliser un htcp qui contient par exemple RCT et RRT, mais ne prendre
# que RRT en compte dans le calcul (cf exemple 3)

# exemple 1 : htcp contient RCT, le calcul de TR prend en compte RCT
htrdr-atmosphere -v                               \
  -a $HTSPK/ecrad_opt_prop.txt                    \
  -c nuages_eau_liquide.htcp                      \
  -m name=RCT:radprop=$HTSPK/Mie_LUT_Cloud-LSW-veff0.1.nc \
  -i def=320x320:spp=2048                             \
  -C pos=3200,3200,0:tgt=3200,3200,1:fov=150:up=0,1,0 \
  -o image_nuages_eau_liquide.ht

# exemple 2 : htcp contient RCT et RRT, le calcul de TR prend en compte RCT et RRT
htrdr-atmosphere -v                               \
  -a $HTSPK/ecrad_opt_prop.txt                    \
  -c nuages_eau_liquide_et_pluie.htcp             \
  -m name=RCT:radprop=$HTSPK/Mie_LUT_Cloud-LSW-veff0.1.nc \
  -m name=RRT:radprop=$HTSPK/Mie_LUT_Cloud-RSW-veff0.1.nc \
  -i def=320x320:spp=2048                             \
  -C pos=3200,3200,0:tgt=3200,3200,1:fov=150:up=0,1,0 \
  -o image_nuages_eau_liquide_et_pluie.ht

# exemple 3 : htcp contient RCT et RRT, le calcul de TR ne prend en compte que RRT
htrdr-atmosphere -v                               \
  -a $HTSPK/ecrad_opt_prop.txt                    \
  -c nuages_eau_liquide_et_pluie.htcp             \
  -m name=RRT:radprop=$HTSPK/Mie_LUT_Cloud-RSW-veff0.1.nc \
  -i def=320x320:spp=2048                             \
  -C pos=3200,3200,0:tgt=3200,3200,1:fov=150:up=0,1,0 \
  -o image_pluie.ht
