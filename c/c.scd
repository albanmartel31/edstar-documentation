c(7e)

# NAME
c - Langage C

# DESCRIPTION
Un lieu pour documenter quelques pratiques courantes du langage C, et de la
compilation de programmes écrits en C.

# VOIR AUSSI

*openmp*(7e)
