# Se référer au fichier COPYRIGHT pour les informations sur la propriété
# patrimoniale et au fichier LICENCE pour les conditions d'utilisation et de
# redistribution du présent fichier.
.POSIX:
.SUFFIXES:

PREFIX = /usr/local

BASH_COMPLETIONS = man

SHELL_SRCS =\
 unix/atelier/base/asciidoc2scdoc.sh\
 unix/atelier/base/unzip-vers-tmp.sh\
 unix/atelier/systeme/audio/analyser-les-cartes-son.bash\
 unix/atelier/systeme/audio/carteSonUsbReconnue.bash\
 unix/atelier/systeme/audio/configurer-les-cartes-son.bash\
 unix/atelier/systeme/audio/configurer-la-carte-son-2i2.sh\
 unix/atelier/systeme/audio/configurer-la-carte-son-4i4.sh\
 unix/atelier/systeme/audio/configurer-la-carte-son-H4.sh\
 unix/atelier/systeme/audio/configurer-la-carte-son-H5.sh\
 unix/atelier/systeme/audio/identifier-les-cartes-son-bluetooth.sh\
 unix/atelier/systeme/bluetooth/appairer-un-peripherique-bluetooth.bash\
 unix/atelier/systeme/bluetooth/connecter-un-des-peripheriques-appaires.bash\
 unix/atelier/systeme/bluetooth/deconnecter-un-peripherique-bluetooth.bash\
 unix/atelier/systeme/disque-externe/demonter-un-disque-externe.bash\
 unix/atelier/systeme/disque-externe/monter-un-disque-externe.bash\
 unix/atelier/systeme/ecran/analyser-les-ecrans.bash\
 unix/atelier/systeme/ecran/configurer-les-ecrans.bash\
 unix/atelier/systeme/udev/lire-une-variable-udev.bash\
 unix/atelier/systeme/udev/zzz-01-disque-externe-enlever-partition.bash\
 unix/atelier/systeme/udev/zzz-01-disque-externe-enlever-peripherique.bash\
 unix/atelier/systeme/udev/zzz-01-disque-externe-info-partition.bash\
 unix/atelier/systeme/udev/zzz-01-disque-externe-info-peripherique.bash\
 unix/atelier/systeme/udev/zzz-03-son-externe-enlever.bash\
 unix/atelier/systeme/udev/zzz-03-son-externe-info.bash\
 unix/atelier/systeme/wifi/ajouter-une-connexion-wifi.bash\
 unix/atelier/systeme/wifi/enlever-une-connexion-wifi.bash

MAN1E_SRCS=\
 unix/base/dd.scd\
 unix/base/diff.scd\
 unix/base/dwm.scd\
 unix/base/latex2html.scd\
 unix/base/patch.scd\
 unix/atelier/base/asciidoc2scdoc.scd\
 unix/atelier/base/unzip-vers-tmp.scd\
 unix/atelier/base/geometries/mkbox-inv.scd\
 unix/atelier/base/geometries/mkbox.scd\
 unix/atelier/base/geometries/mkxmax-inv.scd\
 unix/atelier/base/geometries/mkxmax.scd\
 unix/atelier/base/geometries/mkxmin-inv.scd\
 unix/atelier/base/geometries/mkxmin.scd\
 unix/atelier/base/geometries/mkymax-inv.scd\
 unix/atelier/base/geometries/mkymax.scd\
 unix/atelier/base/geometries/mkymin-inv.scd\
 unix/atelier/base/geometries/mkymin.scd\
 unix/atelier/base/geometries/mkzmax-inv.scd\
 unix/atelier/base/geometries/mkzmax.scd\
 unix/atelier/base/geometries/mkzmin-inv.scd\
 unix/atelier/base/geometries/mkzmin.scd\
 unix/base/asciinema.scd\
 unix/base/base-audio/gstreamer.scd\
 unix/base/base-audio/mumble.scd\
 unix/base/bash.scd\
 unix/base/drawpile.scd\
 unix/base/find.scd\
 unix/base/gdb.scd\
 unix/base/git.scd\
 unix/base/history.scd\
 unix/base/iconv.scd\
 unix/base/irssi.scd\
 unix/base/lftp.scd\
 unix/base/mplayer.scd\
 unix/base/mupdf.scd\
 unix/base/noweb.scd\
 unix/base/old-asciidoc-stephane.scd\
 unix/base/openbox.scd\
 unix/base/shellcheck.scd\
 unix/base/ssh.scd\
 unix/base/tigervnc.scd\
 unix/base/tmate.scd\
 unix/base/vim.scd\
 unix/base/xrandr.scd\
 unix/base/sshfs.scd\
 unix/base/gpg.scd\
 unix/base/pass.scd\

MAN5E_SRCS=\
 unix/base/html.scd\
 unix/base/posix-shell.scd\

MAN7E_SRCS=\
 c/c.scd\
 c/openmp.scd\
 edstar-documentation.scd\
 services-edstar/services-git-edstar.scd\
 stardis/installation-stardis.scd\
 star-cad/installation-star-cad.scd\
 star-engine/star-4v_s.scd\
 star-engine/utilisation-star-engine.scd\
 unix/atelier/atelier.scd\
 unix/atelier/base/latex/latex-atelier.scd\
 unix/atelier/base/partage-ecran/partage-ecran.scd\
 unix/atelier/base/web/meso-web.scd\
 unix/atelier/systeme/audio/alsa-atelier.scd\
 unix/atelier/systeme/audio/son-atelier.scd\
 unix/atelier/systeme/bluetooth/bluetooth-atelier.scd\
 unix/atelier/systeme/disque-externe/disque-externe-atelier.scd\
 unix/atelier/systeme/ecran/xrandr-atelier.scd\
 unix/atelier/systeme/openbox/openbox-atelier.scd\
 unix/atelier/systeme/udev/udev-atelier.scd\
 unix/atelier/systeme/wifi/wifi-atelier.scd\
 unix/base/asciidoc.scd\
 unix/base/base-audio/alsa.scd\
 unix/base/base-audio/jack.scd\
 unix/base/base.scd\
 unix/base/firefox.scd\
 unix/base/gnuplot.scd\
 unix/base/latex.scd\
 unix/serveur/gentoo/ssh-serveur-gentoo.scd\
 unix/serveur/serveur.scd\
 unix/systeme/gentoo/alsa-gentoo.scd\
 unix/systeme/gentoo/amd64-gentoo.scd\
 unix/systeme/gentoo/asciinema-gentoo.scd\
 unix/systeme/gentoo/bluetooth-gentoo.scd\
 unix/systeme/gentoo/ccache-gentoo.scd\
 unix/systeme/gentoo/cle-usb-bootable-gentoo.scd\
 unix/systeme/gentoo/crossdev-gentoo.scd\
 unix/systeme/gentoo/cups-gentoo.scd\
 unix/systeme/gentoo/distcc-gentoo.scd\
 unix/systeme/gentoo/dm-crypt-gentoo.scd\
 unix/systeme/gentoo/drawpile-gentoo.scd\
 unix/systeme/gentoo/edwm-gentoo.scd\
 unix/systeme/gentoo/elogind-gentoo.scd\
 unix/systeme/gentoo/gentoo.scd\
 unix/systeme/gentoo/intel-microcode-gentoo.scd\
 unix/systeme/gentoo/installation-gentoo-depuis-la-cle-usb.scd\
 unix/systeme/gentoo/irssi-gentoo.scd\
 unix/systeme/gentoo/jack-gentoo.scd\
 unix/systeme/gentoo/laptop_mode-gentoo.scd\
 unix/systeme/gentoo/latex-gentoo.scd\
 unix/systeme/gentoo/mumble-gentoo.scd\
 unix/systeme/gentoo/noweb-gentoo.scd\
 unix/systeme/gentoo/openbox-gentoo.scd\
 unix/systeme/gentoo/pinephone-gentoo.scd\
 unix/systeme/gentoo/power-management-gentoo.scd\
 unix/systeme/gentoo/printing-gentoo.scd\
 unix/systeme/gentoo/raspi-gentoo.scd\
 unix/systeme/gentoo/sdcard-gentoo.scd\
 unix/systeme/gentoo/ssh-gentoo.scd\
 unix/systeme/gentoo/sxmo-gentoo.scd\
 unix/systeme/gentoo/tigervnc-gentoo.scd\
 unix/systeme/gentoo/tmate-gentoo.scd\
 unix/systeme/gentoo/X-gentoo.scd\
 unix/systeme/gentoo/x86-64-gentoo.scd\
 unix/systeme/acl.scd\
 unix/systeme/image-disque.scd\
 unix/systeme/pinephone-postmarketos.scd\
 unix/systeme/printing.scd\
 unix/systeme/st.scd\
 unix/systeme/systeme.scd\
 unix/systeme/ubuntu/drawpile-ubuntu.scd\
 unix/systeme/udev.scd\
 unix/unix.scd

MAN8E_SRCS=\
 unix/systeme/cryptsetup.scd\
 unix/systeme/wpa_supplicant.scd

MAN1E = $(MAN1E_SRCS:.scd=.1e)
MAN5E = $(MAN5E_SRCS:.scd=.5e)
MAN7E = $(MAN7E_SRCS:.scd=.7e)
MAN8E = $(MAN8E_SRCS:.scd=.8e)
MAN = $(MAN1E) $(MAN5E) $(MAN7E) $(MAN8E)

build: $(MAN)

.SUFFIXES: .scd .1e .5e .7e .8e

.scd.1e .scd.5e .scd.7e .scd.8e:
	@printf "scdoc < %s > %s\n" $< $@
	@scdoc < $< > $@ || (rm -f $@; exit 1)

clean:
	@rm -f $(MAN1E) $(MAN5E) $(MAN7E) $(MAN9)

install: $(MAN)
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(PREFIX)/share/bash-completion/completions
	@mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1e
	@mkdir -p $(DESTDIR)$(PREFIX)/share/man/man5e
	@mkdir -p $(DESTDIR)$(PREFIX)/share/man/man7e
	@mkdir -p $(DESTDIR)$(PREFIX)/share/man/man8e
	@$(SHELL) make.sh install_bash_completion $(DESTDIR)$(PREFIX) $(BASH_COMPLETIONS)
	@$(SHELL) make.sh install_shell $(DESTDIR)$(PREFIX) $(SHELL_SRCS)
	@$(SHELL) make.sh install_man $(DESTDIR)$(PREFIX) $(MAN)

uninstall:
	@$(SHELL) make.sh uninstall_bash_completion $(DESTDIR)$(PREFIX) $(BASH_COMPLETIONS)
	@$(SHELL) make.sh uninstall_shell $(DESTDIR)$(PREFIX) $(SHELL_SRCS)
	@$(SHELL) make.sh uninstall_man $(DESTDIR)$(PREFIX) $(MAN)
