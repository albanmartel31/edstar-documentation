edstar-documentation(7e)

# NAME
edstar-documentation - Description de edstar-documentation

# INSCRIPTION

Pour accéder au service, envoyer une clé ssh publique à l'un des
administrateurs avec la règle de nommage _id_rsa_gitlab_edstar_prénom_nom.pub_
(voir *services-git-edstar*(7e)).

# DESCRIPTION

*edstar-documentation* est un ensemble de textes ecrits par les membres de la
plateforme *edstar*. Il s'agit de documenter les pratiques récurentes au sein
de la plateforme en termes de *calcul scientifique*, de développement de type
*unix* et d'organisation d'une *pratique collective* de la recherche dans ces
domaines. *edstar-documentation* peut être vu comme un cahier de travaux
pratiques :

	- il n'est question que de découvrir/partager (et élaborer plus avant) une
	  pratique ;
	- chacun des concepts abordés est supposé avoir été (ou devoir être)
	  introduit dans le cadre d'un cours (on admet donc que le cours existe et
	  on ne propose éventuellement que quelques rappels de cours très
	  sommaires) ;
	- le choix des outils est guidé par l'objectif d'illustrer la pratique et
	  aucun d'eux n'est décrit dans l'ensemble de ses fonctionnalités (on admet
	  que pour chaque outil une documentation complète existe par ailleurs).

Les sources de cette documentation sont organisées dans un ensemble de
répertoires. Certains portent le nom des principaux projets portés par la
plateforme : _star-engine_, _stardis_, _htrdr_, _solstice_, _star-schiff_. Les
autres concernent l'organisation de la pratique collective :

	- _unix/base_ décrit les outils unix usuels utilisés sur la plateforme ;
	- _unix/systeme_ décrit les travaux d'administration système ;
	- _unix/serveur_ décrit la gestion des serveurs ;
	- _unix/atelier_ décrit les outils unix que nous avons développés, par
	  combinaison des outils unix de base, dans le but de fluidifier les
	  pratiques spécifiques de la plateforme.

Les fichiers source de la documentation utilisent le langage de balisage léger
*scdoc*(5) pour décrire le contenu de pages de manuel destinées à être lues par
le programme *man*(1). Le programme *scdoc*(1) est utilisé pour générer les
pages de man à partir des fichiers source. Une fois installées sur le système,
ces pages sont organisées en _sections_. *edstar-documentation* n'utilise pas
les sections couramment définies sur un système UNIX : le nom de ses sections
débute par un chiffre faisant référence aux sections *man*(1) classiques mais
suivi du caractère 'e' renvoyant au 'e' d'*edstar-documentation*. Ci dessous
nous listons les principales sections utilisées et le type de page qu'elles
contiennent :

	- _1e_ programmes exécutables et script shells
	- _5e_ formats de fichiers
	- _7e_ divers
	- _8e_ commandes d'administration système

À noter que dans l'ensemble d'edstar-documentation, nous faisons référence à
un utilisateur fictif nommé _lafrier_.

# UTILISATION

Avant d'être consultées, les pages de manuel doivent être générées à partir
des fichiers source, puis installées sur le système. Pour ce faire
*edstar-documentation* propose un fichier _Makefile_ au standard POSIX (voir
*make*(1p)) automatisant entre autre le processus de génération des pages de
manuel et leur installation. En assumant que le programme *scdoc* [1] est
installé sur le système (pour éviter les problèmes de compilation, il est conseillé d'installer la dernière version disponible sur *https://git.sr.ht/~sircmpwn/scdoc*), l'utilisateur peut alors générer puis installer les
pages de manuel comme suit :

```
lafrier ~ $  make
lafrier ~ $  make PREFIX=~/.local/ install
```

Ici nous choisissons d'installer les pages de manuel dans le répertoire local
_~/.local_ en redéfinissant la variable _PREFIX_ dont la valeur par défaut est
_/usr/local/_.

Pour accéder aux pages de manuel que nous venons d'installer nous devons nous
assurer que la variable d'environnement _MANPATH_ liste bien le répertoire qui
contient lesdites pages et, dans le cas contraire, venir l'enrichir. Pour ce
faire, une solution simple consiste à venir éditer le script shell qui
personnalise l'environnement de l'interpréteur de commande courant. Par
exemple, pour l'interpréteur de commande GNU Bash :

```
lafrier ~ $  vi ~/.bashrc
vi:
  export MANPATH=$MANPATH:~/.local/share/man/
  export PATH=~/.local/bin/:$PATH
lafrier ~ $  . ~/.bashrc
```

En plus de la variable _MANPATH_ nous modifions aussi la variable _PATH_ pour
lui ajouter le répertoire dans lequel edstar-documentation installe ses
programmes exécutables.

Pour consulter un page de manuel, par exemple celle de la page de *ssh*, ne reste
plus qu'à invoquer la commande *man*(1) :

```
lafrier$  man 1e ssh
```

# LICENCE

*edstar-documentation* est un logiciel libre ; vous pouvez le redistribuer ou
le modifier suivant les termes de la GNU General Public License telle que
publiée par la Free Software Foundation ; soit la version 3 de la licence,
soit (à votre gré) toute version ultérieure. Ce programme est distribué dans
l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la garantie
tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la
GNU General Public License pour plus de détails. Vous devez avoir reçu une
copie de la GNU General Public License en même temps que ce programme ; si ce
n'est pas le cas, consultez <http://www.gnu.org/licenses>.

# VOIR AUSSI

. https://git.sr.ht/~sircmpwn/scdoc

*make*(1p), *scdoc*(5), *services-git-edstar*(7e)
