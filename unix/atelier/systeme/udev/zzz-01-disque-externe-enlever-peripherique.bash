#!/bin/bash
nomPeripherique="$1"

# On efface le fichier d'information correspondant au périphérique.
rm "/tmp/disqueExterne-$nomPeripherique.bash"

# Debug
#/bin/echo "enlever peripherique : $@" >> /tmp/zzz-99-test.txt
