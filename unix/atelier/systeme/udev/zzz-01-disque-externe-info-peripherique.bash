#!/bin/bash
nomPeripherique="$1"
nomVendeur="$2"
nomModele="$3"
nomNom="$4"

# On écrit les informations dans un tableau associatif.
declare -A disque

disque["peripherique"]="$nomPeripherique"
disque["partition"]="vide"
disque["vendeur"]="$nomVendeur"
disque["modele"]="$nomModele"
disque["nom"]="$nomNom"
disque["systemeDeFichier"]="vide"
disque["lien"]="$nomPeripherique"
disque["pointDeMontage"]="vide"

# On écrit le tableau associatif dans le fichier d'information et on donne les droits de lecture et d'écriture à tous les utilisateurs.
declare -p disque > "/tmp/disqueExterne-$nomPeripherique.bash"
chmod 666 "/tmp/disqueExterne-$nomPeripherique.bash"

# Debug
#/bin/echo "disque peripherique : $@" >> /tmp/zzz-99-test.txt
