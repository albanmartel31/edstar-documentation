#/bin/bash
# On teste si le fichier ~/.asoundrc est bien un lien symbolique (sinon on ne l'écrase pas).
if [ ! -L ~/.asoundrc ]
then
  echo "Le fichier ~/.asoundrc n'est pas un lien symbolique. On conserve ce fichier et on ne lance donc aucune configuration. Si vous souhaitez lancer la configuration, il faut d'abord déplacer ce fichier."
else
  echo "Les cartes son listées ci-dessus sont disponibles. Souhaitez-vous utiliser une de ces cartes~? [entrez le numéro du début de ligne, sinon entrez 0]"
  nombreCartesSonUsbConnues="4"
  echo "1 - scarlett 2i2"
  echo "2 - scarlett 4i4"
  echo "3 - zoom H4"
  echo "4 - zoom H5"
  nombreCartesSonDisponibles="$nombreCartesSonUsbConnues"
  # On va chercher les cartes son bluetooth
  identifier-les-cartes-son-bluetooth.sh
  source "$HOME/.config/bluetooth/adresse_mac_carte_son_bluetooth.bash"
  nombreCartesSonBluetooth="${#carteSonBluetooth[@]}"
  for nomCarteBluetooth in "${!carteSonBluetooth[@]}"
  do
    numero="$( echo "$nombreCartesSonDisponibles + 1" | bc )"
    echo "$numero - $nomCarteBluetooth"
    nombreCartesSonDisponibles="$numero"
  done
  read reponse
  # On regarde si la réponse fait partie des cartes son USB connues. Si oui on configure la carte son.
  if [ "$reponse" = "1" ]
  then
    configurer-la-carte-son-2i2.sh
  fi
  if [ "$reponse" = "2" ]
  then
    configurer-la-carte-son-4i4.sh
  fi
  if [ "$reponse" = "3" ]
  then
    configurer-la-carte-son-H4.sh
  fi
  if [ "$reponse" = "4" ]
  then
    configurer-la-carte-son-H5.sh
  fi
  # On regarde si la réponse fait partie des cartes son bluetooth. Si oui on la configure.
  nombreCartesSonDisponibles="$nombreCartesSonUsbConnues"
  for nomCarteBluetooth in "${!carteSonBluetooth[@]}"
  do
    numero="$( echo "$nombreCartesSonDisponibles + 1" | bc )"
    if [ "$reponse" = "$numero" ]
    then
      cat "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/asoundrc-bluetooth | sed "s/adresse_mac_peripherique_bluetooth/${carteSonBluetooth["$nomCarteBluetooth"]}/" > ~/.asoundrc-bluetooth-"${carteSonBluetooth["$nomCarteBluetooth"]}"
      rm ~/.asoundrc
      ln -s ~/.asoundrc-bluetooth-"${carteSonBluetooth["$nomCarteBluetooth"]}" ~/.asoundrc
    fi
    nombreCartesSonDisponibles="$numero"
  done
fi
