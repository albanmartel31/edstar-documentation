#!/bin/bash
# On lit la liste des periphériques bluetooth appairés, on ne retient que les cartes son, on regarde si elles sont connectées, et on les écrit dans un vecteur associatif.
declare -A carteSonBluetooth
source "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"
for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
do
  icon=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Icon/ {print $2}' )
  connected=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Connected/ {print $2}' )
  # Si le periphérique est un périphérique audio et si il est connecté, alors on le rajoute à la liste des cartes son disponibles
  if [ "$icon" = "audio-card" ] && [ "$connected" = "yes" ]
  then
    carteSonBluetooth["$nom"]="${adresse_mac_peripherique_bluetooth[$nom]}" 
  fi
  if [ "$icon" = "audio-headset" ] && [ "$connected" = "yes" ]
  then
    carteSonBluetooth["$nom"]="${adresse_mac_peripherique_bluetooth[$nom]}" 
  fi
done
# On écrit le vecteur associatif dans un fichier
declare -p carteSonBluetooth > "$HOME/.config/bluetooth/adresse_mac_carte_son_bluetooth.bash"

