#!/bin/bash
# On liste les fichiers d'information existant dans le répertoire `/tmp` et on les met dans la variable `fichiersInfo`.
fichiersInfo=($(ls /tmp/disqueExterne-*.bash 2>/dev/null))
if [ "${#fichiersInfo[@]}" = "0" ]
then
# Il n'y a aucun fichier d'information donc aucun disque externe n'est branché
  echo "Il n'y a aucun disque externe branché sur l'ordinateur"
else
# Il y a des fichiers d'information : on les passe en revue et on repère ceux qui indique une partition qui est déjà montée.
  declare -A disque
  declare -A fichiersInfoMontes
  nombre="0"
  for i in "${fichiersInfo[@]}"
  do
  # Boucle sur les fichiers d'information
    source "$i"
    # On teste si il s'agit d'une partition et si elle est déjà montée
    if [ "${disque[partition]}" != "vide" ] && [ "${disque[pointDeMontage]}" != "vide" ]
    then
      # On incrémente le compteur 
      nombre="$( echo "$nombre + 1" | bc )"
      # On archive le nom du fichier d'information dans un tableau associatif [numero -> nomdu fichier]
      numero="$nombre"
      fichiersInfoMontes["$numero"]="$i"
      # On affiche les informations sur la partition
      echo "$numero : /dev/${disque[lien]} est monté. Son point de montage est ${disque[pointDeMontage]}. [ nom : ${disque[nom]} , modèle : ${disque[modele]} , vendeur : ${disque[vendeur]} , système de fichier : ${disque[systemeDeFichier]} ]"
    fi
  done
  if [ "$nombre" = 0 ]
  then
    echo "Aucune partition n'est montée : soit les disques branchés n'ont pas de partition, soit les partitions ne sont pas encore montées. Vous pouvez éventuellement voir les partitions disponibles au montage (et éventuellement les monter) avec la commande monter-un-disque-externe.bash."
  else
    echo "Quelle partition voulez-vous demonter dans la liste ci-dessus ? [entrez le numéro du début de ligne]"
    read reponse
    reponseValable=0
    for i in "${!fichiersInfoMontes[@]}"
    do
      if [ "$reponse" = "$i" ]
      then
        reponseValable=1
      fi
    done
    if [ "$reponseValable" = "1" ]
    then
      # On lit le fichier d'information de la partition choisie 
      source "${fichiersInfoMontes[$reponse]}"
      # On démonte la partition
      sudo umount "${disque[pointDeMontage]}"
      # On efface le point de montage dans le fichier d'information 
      disque[pointDeMontage]="vide"
      declare -p disque > "${fichiersInfoMontes[$reponse]}"
    fi
  fi
fi
