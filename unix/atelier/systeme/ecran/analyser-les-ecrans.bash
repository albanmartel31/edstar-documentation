#!/bin/bash
# On recuppère la sortie de `xrandr` et on enlève les lignes conntenant `disconnected` ou `Screen` ou `Hz`. On met le résultat dans le fichier `/tmp/informations-ecrans.txt`. 
xrandr --query | sed '/disconnected/d' | sed '/Screen/d' | sed '/Hz/d' > /tmp/informations-ecrans.txt
# On lit la première colonne des lignes contenant `connected` pour trouver les noms des écrans accessibles.
nomsDesEcrans=$( cat /tmp/informations-ecrans.txt | grep connected | awk '{print $1}' )
# On lit les informations concernant chaque écran.
nombreEcrans="0"
declare -A ecran
for nomEcran in $nomsDesEcrans
do
  # On compte le nombre d'écrans.
  nombreEcrans="$( echo "$nombreEcrans + 1" | bc )"
  # On archive le nom de l'écran.
  ecran["$nombreEcrans"]="$nomEcran"
  # On crée un vecteur associatif pour chaque écran qui relie une résolution à une série de fréquences.
  nomDuTableauAssociatif="ecran$nombreEcrans"
  declare -A "$nomDuTableauAssociatif"
  #declare -A ecran1
  # Lors de la lecture ligne à ligne du fichier `/tmp/informations-ecrans.txt`, la variable `avant` vaut `1` si on n'est pas encore parvenu aux lignes concernant l'écran dont le nom est `nomEcran`. De même variable `après` vaut `1` si on a dépassé les lignes concernant l'écran dont le nom est `nomEcran`.
  avant="1"
  apres="0"
  # Lecture ligne à ligne du fichier `/tmp/informations-ecrans.txt`.
  while read ligne
  do
    # Si `apres` vaut `1` on ne fait rien 
    if [ "$apres" != "1" ]
    then
      # On regarde si la ligne est une ligne d'entète.
      test=$(echo $ligne | grep connected)
      if [ "$test" != "" ]
      then
        if [ "$avant" = "0" ]
        then
          # Comme `avant` vaut `0`, on sait que l'on en était à lire les informations de l'écran dont le nom est `nomEcran`. On trouve une ligne d'entète, donc la lecture des informations est terminée.
          apres="1"
        else
          # On regarde si la ligne d'entète est celle de l'écran dont le nom est `nomEcran`.
          nom="$(echo "$ligne" | awk '{print $1}')"
          if [ "$nom" = "$nomEcran" ]
          then
            # La ligne d'entète est la bonne.
            avant="0"
          fi
        fi
      else
        # La ligne n'est pas une ligne d'entète, mais une ligne de description d'une résolution.
        if [ "$avant" = "0" ]
        then
          # Il s'agit bien d'une information concernant l'écran dont le nom est `nomEcran`. On archive donc cette information.
          # On lit la résolution.
          resolution="$(echo "$ligne" | awk '{print $1}')"
          # On lit les fréquences de balayage corespondant à cette résolution.
          frequence="$(echo "$ligne" | awk '{$1=""; print}')"
          # On archive cette information de résolution et de fréquence dans le tableau associatif.
          eval "$nomDuTableauAssociatif[\"$resolution\"]=\"$frequence\""
        fi
      fi
    fi
  done < /tmp/informations-ecrans.txt
done
# Pour chaque écran, on enlève les résolutions non petinentes et on écrit les tableaux associatis dans un fichier.
for numero in "${!ecran[@]}"
do
  nomDuTableauAssociatif="ecran$numero"
  # On cherche la résolution conseillée.
  eval "listeDesResolutions=\${!$nomDuTableauAssociatif[@]}"
  for resolution in $listeDesResolutions
  do
    eval "frequences=\${$nomDuTableauAssociatif["$resolution"]}"
    test="$(echo "$frequences" | grep -c +)"
    if [ "$test" != "0" ]
    then
      resolutionConseillee="$resolution"
      # On enlève le `x` de façon à pouvoir séparer les résolutions horizontales et verticales. On enlève aussi les `i` (pour `interlacé`) et les `p`.
      tmp="$(echo "$resolutionConseillee" | sed -e $'s/x/\ /g' | sed -e $'s/i/\ /g'| sed -e $'s/p/\ /g')"
      resxConseillee="$(echo "$tmp" | awk '{print $1}')"
      resyConseillee="$(echo "$tmp" | awk '{print $2}')"
    fi
  done
  # On enlève les résolutions qui n'ont pas le même rapport de forme que celui de la résolution conseillée.
  for resolution in $listeDesResolutions
  do
    eval "frequences=\${$nomDuTableauAssociatif["$resolution"]}"
    # On enlève le `x` de façon à pouvoir séparer les résolutions horizontales et verticales. On enlève aussi les `i` (pour `interlacé`) et les `p`.
    tmp="$(echo "$resolution" | sed -e $'s/x/\ /g' | sed -e $'s/i/\ /g'| sed -e $'s/p/\ /g')"
    resx="$(echo "$tmp" | awk '{print $1}')"
    resy="$(echo "$tmp" | awk '{print $2}')"
    # On teste si le rapport de forme est bien le même que celui de la résolution conseillée. Si ce n'est pas le cas, on retire la résolution de la liste.
    test="$( echo "$resxConseillee*$resy-$resyConseillee*$resx" | bc )"
    if [ "$test" != "0" ]
    then
      eval "unset $nomDuTableauAssociatif["$resolution"]"
    fi
  done
  # On écrit le tableau associatif dans un fichier.
  eval "declare -p $nomDuTableauAssociatif > /tmp/$nomDuTableauAssociatif.bash"
done
# On écrit les noms des écrans dans un fichier.
declare -p ecran > /tmp/ecran.bash

