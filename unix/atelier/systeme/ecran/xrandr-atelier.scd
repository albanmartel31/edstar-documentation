xrandr-atelier(7e)

# NAME
xrandr-atelier - Configuration des écrans

# INTRODUCTION
Les programmes _bash_ décrits ci-dessous utilisent l'utilitaire _xrandr_ :

	- _analyser-les-ecrans.bash_ lit les caractéristiques des écrans disponibles (nom et résolutions) et les écrit sous la forme de tableaux associatifs dans les fichiers _/tmp/ecran.bash_, _/tmp/ecran1.bash_, _/tmp/ecran2.bash_, etc.
	- _configurer-les-ecrans.bash_ permet de configurer les écrans (choisir ceux qui sont activés, leur position, leur résolution).

# INSTALLATION
Les deux programmes _bash_ décrit ci-dessus doivent avoir les droits d'execution et être recopiés dans '$EDSTARDOCUMENTATION/unix/atelier/bin'.

# VOIR AUSSI
*xrandr*(1e) *xrandr*(1)
