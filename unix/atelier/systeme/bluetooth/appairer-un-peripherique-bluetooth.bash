#!/bin/bash
echo "Analyse en cours ..."
pipe=/tmp/btctlpipe 
output_file=/tmp/btctl_output

if [[ ! -p $pipe ]]; then
  mkfifo $pipe
fi

trap terminate INT
function terminate()
{
  killall bluetoothctl &>/dev/null
  rm -f $pipe
}

function bleutoothctl_reader() 
{
  {
    while true
    do
      if read line <$pipe; then
          if [[ "$line" == 'exit' ]]; then
              break
          fi          
          echo $line
      fi
    done
  } | bluetoothctl > "$output_file"
}


function bleutoothctl_writer() 
{
  cmd=$1
  printf "$cmd\n\n" > $pipe
}

bleutoothctl_reader &
sleep 1
bleutoothctl_writer "scan on"
sleep 15
bleutoothctl_writer "scan off"
sleep 1
bleutoothctl_writer "devices"
sleep 1
bleutoothctl_writer "exit"

device_list=$(cat $output_file | grep -e '^Device.*' | sed 's/Device //g')

# On écrit les périphériques bluetooth trouvés dans une table

OLDIFS=$IFS
IFS=$'\n'
table_peripherique=( $(echo "$device_list") )
IFS=$OLDIFS
nombre_peripherique="${#table_peripherique[@]}"
for i in $( seq "$nombre_peripherique" )
do
  indice="$( echo "$i - 1" | bc )"
  echo "$i : ${table_peripherique[$indice]}"
done
# On demande le périphérique bluetooth a configurer
echo "Les périphériques bluetooth ci-dessus sont disponibles. Lequel souhaitez-vous configurer ? [entrez le numéro du début de ligne]"
read reponse
reponseValable="0"
for i in $( seq "$nombre_peripherique")
do
  if [ "$reponse" = "$i" ]
  then
    reponseValable="1"
  fi
done
if [ "$reponseValable" = "1" ]
then
  # On lit le numero mac et le nom du périphérique choisi
  indice_peripherique_choisi="$( echo "$reponse - 1" | bc )"
  ligne_peripherique_choisi=( $(echo "${table_peripherique[$indice_peripherique_choisi]}") )
  numero_mac_peripherique_choisi="${ligne_peripherique_choisi[0]}"
  nom_peripherique_choisi=""
  for i in $( seq 2 "${#ligne_peripherique_choisi[@]}" )
  do
    indice="$( echo "$i - 1" | bc )"
    nom_peripherique_choisi="$( echo $nom_peripherique_choisi ${ligne_peripherique_choisi[$indice]} )" 
  done
fi
# On appaire le peripherique choisi
bleutoothctl_reader &
sleep 1
bleutoothctl_writer "pair $numero_mac_peripherique_choisi"
sleep 1
bleutoothctl_writer "trust $numero_mac_peripherique_choisi"
sleep 1
bleutoothctl_writer "exit"
# On lit le fichier contenant la liste des peripheriques bluetooth appairés (si il existe)
declare -A adresse_mac_peripherique_bluetooth
if [ -e "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash" ]
then
  source "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"
fi
# On rajoute le nouveau peripherique (ou on écrase son adresse mac si il existe déjà)
adresse_mac_peripherique_bluetooth["$nom_peripherique_choisi"]="$numero_mac_peripherique_choisi"
declare -p adresse_mac_peripherique_bluetooth > "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"

terminate



