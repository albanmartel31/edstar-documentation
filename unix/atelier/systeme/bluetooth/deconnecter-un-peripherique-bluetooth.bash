#!/bin/bash
# On lit la liste des periphériques bluetooth appairés
source "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"
# On affiche les périphériques bluetooth connectés
numero="0"
for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
do
  connected=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Connected/ {print $2}' )
  if [ "$connected" = "yes" ]
  then
    numero="$( echo "$numero + 1" | bc )"
    echo "$numero : $nom"
  fi
done
# Si il y a au moins un périphériques bluetooth connecté, on demande quel périphérique il faut déconnecter
if [ "$numero" != "0" ]
then
  echo "Les périphériques bluetooth ci-dessus sont connectés. Lequel souhaitez-vous déconnecter ? [entrez le numéro du début de ligne]"
  read reponse
  reponseValable="0"
  numero="0"
  for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
  do
    connected=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Connected/ {print $2}' )
    if [ "$connected" = "yes" ]
    then
      numero="$( echo "$numero + 1" | bc )"
      if [ "$reponse" = "$numero" ]
      then
        reponseValable="1"
        nom_choisi=$nom
      fi
    fi
  done
  if [ "$reponseValable" = "1" ]
  then
    bluetoothctl disconnect "${adresse_mac_peripherique_bluetooth[$nom_choisi]}"
  fi
fi




