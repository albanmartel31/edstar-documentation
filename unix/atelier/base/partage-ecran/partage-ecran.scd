partage-ecran(7e)

# NAME
partage-ecran - Partage des écrans dans le travail collectif

# DESCRIPTION
Lors des séances de travail, il est souvent nécessaire de partager divers documents via un serveur d'affichage. Pour cela nous utilisons la suite d'outils _VNC_. Remarque :  lorsque l'objectif est seulement de partager un terminal, voir *tmate*(1e).

# LANCER LES SERVICES
On se connecte sur le serveur pour démarre les services _VNC_ via le script _vnc_. Ce script démarre :

- un serveur _X_ (qui lance le système de fenêtrage _dwm_) ;
- un serveur _VNC_ qui écoute le port 5900 en lecture seule ;
- un serveur _VNC_ qui écoute le port 5901 en lecture-écriture ;
- un serveur _noVNC_ qui expose via _https_ (port 443) le serveur _VNC_ qui écoute le port 5900.

Le port 5900 sera accessible publiquement via l'url _https://vnc.meso-star.com_. Les ports 5900 et 5901 sont aussi accessibles par tunel _ssh_.

```
lafrier$  ssh stardis-meso-vps
vps$  ./vnc start
```

# PARTAGER UN CONTENU

## Transférer un fichier
Le contenu à partager graphiquement est en général un fichier au format _pdf_ ou une image (_jpeg_, _png_, _ppm_, etc). On le transfère sur le serveur via *rsync*(1e) :

```
lafrier$  rsync -avr --progress le_fichier stardis-meso-vps:Lafrier/
```

## Partager l'affichage d'un fichier _pdf_
On crée le tunel ssh et on lance _vncviewer_ pour se connecter en lecture-écriture au serveur X.

```
lafrier$  ssh -L 5901:localhost:5901 stardis-meso-vps -fN
lafrier$  vncviewer localhost:5901
```

Sur le serveur X distant (donc sous le système de fenêtrage _dwm_), on ouvre un terminal :

```
Alt+Enter
Alt+m
Alt+b
```

Dans le terminal, on utilise *mupdf*(1e) pour lire le document transféré :

```
vps$  cd Lafrier
vps$  mupdf le_fichier.pdf &
```

Pour basculer de la fenêtre de mupdf au terminal, on utilise _Alt+j_.

# STOPPER LES SERVICES

```
lafrier$  ssh stardis-meso-vps
vps$  ./vnc stop
vps$  killall -u stardis
```

# VOIR AUSSI
*tmate*(1e), *rsync*(1e)

