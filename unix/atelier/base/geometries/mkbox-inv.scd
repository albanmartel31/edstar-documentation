mkbox-inv(1e)

# NAME
mkbox-inv - Utilisation de mkbox-inv

# DESCRIPTION
*mkbox-inv* est une commande permettant de créer un fichier au format *stl* correspondant à une boite parallélépipédique située (dans un repère cartésien Oxyz) entre les coordonnées _x=xmin_ et _x=xmax_ en x, entre les coordonnées _ymin_ et _ymax_ en y et entre les coordonnées _zmin_ et _zmax_ en z. Les normales sont tournées vers l'intérieur.

# UTILISATION
Pour _xmin=4_, _xmax=5_, _ymin=2_, _ymax=6_, _zmin=5_, _zmax=7_ :
```
lafrier$  mkbox-inv 4 5 2 6 5 7
```

# VOIR AUSSI
*mkbox*(1e)
