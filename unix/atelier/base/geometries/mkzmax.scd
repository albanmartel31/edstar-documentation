mkzmax(1e)

# NAME
mkzmax - Utilisation de mkzmax

# DESCRIPTION
*mkzmax* est une commande permettant de créer un fichier au format *stl* correspondant à une surface parallélépipédique située (dans un repère cartésien Oxyz) dans le plan _z=zmax_, entre les cooredonnées _xmin_ et _xmax_ en x et entre les coordonnées _ymin_ et _ymax_ en y. Les normales sont tournées vers les z croissants.

# UTILISATION
Pour _xmin=4_, _xmax=6_, _ymin=2_, _ymax=5_, _zmax=7_ :
```
lafrier$  mkzmax 4 6 2 5 7
```

# VOIR AUSSI
*mkxmin*(1e), *mkxmin-inv*(1e), *mkxmax*(1e), *mkxmax-inv*(1e), *mkymin*(1e), *mkymin-inv*(1e), *mkymax*(1e), *mkymax-inv*(1e), *mkzmin*(1e), *mkzmin-inv*(1e), *mkzmax-inv*(1e)
