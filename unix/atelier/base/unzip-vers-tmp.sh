#!/bin/sh
nomComplet="${1%%.*}"
nom="$(basename $nomComplet)"
repertoire="/tmp/$nom"
#echo "$nomComplet $nom $repertoire"
if ( test -e $nomComplet.zip ) then
  mkdir "$repertoire"
  unzip -d "$repertoire" "$nomComplet.zip"
else
  echo "Le fichier $nom.zip n'existe pas"
fi
