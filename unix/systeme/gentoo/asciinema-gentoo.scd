asciinema-gentoo(7e)

# NAME
asciinema-gentoo - Installer asciinema sur gentoo

# INSTALLATION
```
root#  emerge -av app-misc/asciinema
```

# VOIR AUSSI
*asciinema*(7e)
