power-management-gentoo(7e)

# NAME
power-management-gentoo - Gestion de la consommation électrique sous gentoo

# LE NOYAU
Configurer le noyau de la façon suivante, où on a choisi le gouverneur _performance_, plutôt que _powersave_, _userspace_, _ondemand_ ou _conservative_. Attention! bien activer _performance_ sur la ligne _Default CPUFreq governor_. Penser aussi à regarder si certains choix peuvent être adaptés en fonction de votre processeur :
```
General setup  --->
  [*] Configure standard kernel features (expert users)  --->
Power management and ACPI options  --->
  [*] Suspend to RAM and standby
  [ ]   Skip kernel's sys_sync() on suspend to RAM/standby
  [*] Hibernation (aka 'suspend to disk')
  [*]   Userspace snapshot device
  ()    Default resume partition
  [ ] Opportunistic sleep
  [ ] User space wakeup sources interface
  -*- Device power management core functionality
  [*]   Power Management Debug Support
  [ ]     Extra PM attributes in sysfs for low-level debugging/testing
  [ ]     Test suspend/resume and wakealarm during bootup
  [*] Suspend/resume event tracing
  [ ] Enable workqueue power-efficient mode by default
  [ ] Energy Model for CPUs
  [*] ACPI (Advanced Configuration and Power Interface) Support  --->
    <*>   AC Adapter
    <*>   Battery
    -*-   Button
    -*-   Video
    <*>   Fan
    -*-   Processor
    <*>   Thermal Zone
  [*] Power Management Timer Support
  [ ] SFI (Simple Firmware Interface) Support  ----
      CPU Frequency scaling  --->
        -*- CPU Frequency scaling
        [*]   CPU frequency transition statistics
              Default CPUFreq governor (performance)  --->
        -*-   'performance' governor
        <*>   'powersave' governor
        <*>   'userspace' governor for userspace frequency scaling
        <*>   'ondemand' cpufreq policy governor
        <*>   'conservative' cpufreq governor
        -*-   'schedutil' cpufreq policy governor
              *** CPU frequency scaling drivers ***
        -*-   Intel P state control
        < >   Processor Clocking Control interface driver
        <*>   ACPI Processor P-States driver
        [ ]     Legacy cpb sysfs knob support for AMD CPUs
        < >   AMD Opteron/Athlon64 PowerNow!
        < >   AMD frequency sensitivity feedback powersave bias
        < >   Intel Enhanced SpeedStep (deprecated)
        < >   Intel Pentium 4 clock modulation
              *** shared options ***
      CPU Idle  --->
  [ ] Cpuidle Driver for Intel Processors
Device Drivers  --->
  -*- Thermal drivers  --->
    --- Thermal drivers
    [ ]   Thermal netlink management
    [ ]   Thermal state transition statistics
    (0)   Emergency poweroff delay in milli-seconds
    [*]   Expose thermal sensors as hwmon device
    -*-   Enable writable trip points
          Default Thermal governor (step_wise)  --->
    [*]   Fair-share thermal governor
    -*-   Step_wise thermal governor
    [*]   Bang Bang thermal governor
    -*-   User_space thermal governor
    [ ]   Thermal emulation mode support
          Intel thermal drivers  --->
            <*> Intel PowerClamp idle injection driver
            <*> X86 package temperature thermal driver
            < > Intel SoCs DTS thermal driver
                ACPI INT340X thermal drivers  --->
                  <*> ACPI INT340X thermal drivers
                  <*> ACPI INT3406 display thermal driver
            <*> Intel PCH Thermal Reporting Driver
  [*] Generic powercap sysfs driver  --->
      <*>   Intel RAPL Support via MSR Interface
      [ ]   Idle injection framework
```

# INSTALLATION
## Installer les services
```
root#  emerge -av sys-power/thermald
root#  rc-config add thermald
root#  rc-config start thermald
root#  emerge -av laptop-mode-tools
root#  emerge -av sys-power/acpid
root#  emerge -av sys-power/powerstat
```
La configuration de _laptop_mode_ se fait dans le fichier _/etc/laptop-mode/laptop-mode.conf_ ainsi que dans les fichiers situés dans le répertoire __/etc/laptop-mode/conf.d_. Notamment, pour indiquer un disque SSD nommé _/dev/nvme0n1_ on écrit :
```
root#  nano /etc/laptop-mode/laptop-mode.conf
nano:
  HD="/dev/[hs]d[abcdefgh] /dev/nvme0n1"
```

RQ: dans le fichier _/etc/laptop-mode/conf.d/cpufreq.conf_, écrire _CONTROL_CPU_FREQUENCY=0_ annule le controle de la fréquence du cpu. Ici on la conserve :
```
root#  nano /etc/laptop-mode/conf.d/cpufreq.conf
nano:
  CONTROL_CPU_FREQUENCY="auto"
  #CONTROL_CPU_FREQUENCY=0
```
RQ: dans le fichier _/etc/laptop-mode/conf.d/intel_pstate.conf_, mettre à _1_ les trois _INTEL_PSTATE_NO_TURBO_ :
```
root#  nano /etc/laptop-mode/conf.d/intel_pstate.conf
nano:
  NOLM_AC_INTEL_PSTATE_NO_TURBO=1
  LM_AC_INTEL_PSTATE_NO_TURBO=1
  BATT_INTEL_PSTATE_NO_TURBO=1
```

Quand la configuration est terminée on lance le service :
```
root#  /etc/init.d/acpid start
root#  rc-update add acpid default
root#  /etc/init.d/laptop_mode start
root#  rc-update add laptop_mode default
```

# PRATIQUE

## Batterie
Pou connaitre le niveau de charge de la batterie :
```
lafrier ~ $ acpitool -b
```
