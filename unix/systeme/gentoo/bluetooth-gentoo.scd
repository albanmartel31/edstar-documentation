bluetooth-gentoo(7e)

# NAME
bluetooth-gentoo - Gestion des périphérique bluetooth sur la distribution Gentoo

# DESCRIPTION

Cette documentation décrit la gestion des périphérique _bluetooth_
sous la distribution GNU/Linux Gentoo via le servive _bluetooth_
que l'on configure avec l'utilitaire _bluetoothctl_.

# NOYAU
```
[*] Networking support --->
   <M>   Bluetooth subsystem support --->
      [*]   Bluetooth Classic (BR/EDR) features
      <*>     RFCOMM protocol support
      [*]       RFCOMM TTY support
      <*>     BNEP protocol support
      [*]       Multicast filter support
      [*]       Protocol filter support
      <*>     HIDP protocol support
      [*]     Bluetooth High Speed (HS) features
      [*]   Bluetooth Low Energy (LE) features
      [ ]   Enable LED triggers
      [ ]   Enable Microsoft extensions
      [*]   Export Bluetooth internals in debugfs
      [ ]   Bluetooth self testing support
      [ ]   Enable runtime option for debugging statements
            Bluetooth device drivers  --->
              <M> HCI USB driver
              [*]   Broadcom protocol support
              [*]   Realtek protocol support
              <M> HCI UART driver
   <*>   RF switch subsystem support  --->
Device Drivers  --->
   HID support  --->
      [*]   /dev/hidraw raw HID device support
      <*>   User-space I/O driver support for HID subsystem
      <*>   Generic HID driver
```

# INSTALLATION
On installe _linux-firmware_ si il n'est pas déjà installé, puis _bluez_. Ensuite on recompile tous les paquets avec _bluetooth_ en rajoutant le _USE_ global dans _/etc/portage/make.conf_. Ensuite on lance le service et on rajoute les utilisateurs concernés au groupe _plugdev_.
```
root#  emerge -av sys-kernel/linux-firmware
root#  emerge -av net-wireless/bluez
root#  nano /etc/portage/make.conf
nano:
  USE="${USE} bluetooth"
root#  emerge -av --update --deep --newuse --changed-use @world
root#  rc-service bluetooth start
root#  rc-update add bluetooth default
root#  gpasswd -a lafrier plugdev
```

# UTILISATION
## Préparation
On utilise _bluetoothctl_ en tant qu'utilisateur. La première fois, on liste les controleurs disponibles avec
```
lafrier$  bluetoothctl list
```
Cette commande renvoie les adresses mac des controleurs, par exemple
```
Controller 3C:58:C2:FF:03:29 BlueZ 5.61 [default]
```
Si il y a plusieurs contoleurs, on choisit le controleur par défaut avec par exemple
```
lafrier$  bluetoothctl select 3C:58:C2:FF:03:29
```
Ensuite on prépare le controleur avec les commandes suivantes :
```
lafrier$  bluetoothctl
bluetoothctl:
  power on
  agent on
  default-agent
  discoverable on
  pairable on
```

## Se connecter à un peripherique
Avec _bluetoothctl_ on active le scan, puis on affiche les périphériques repérés :
```
lafrier$  bluetoothctl
bluetoothctl:
  scan on
  devices
```
Par exemple, _devices_ renvoie
```
88:C6:26:44:0C:EA UE BOOM
```
Pour se connecter à ce périphérique, on continue alors sous bluetoothctl. La commande _pair_ permet d'appairer le périphérique. Ce n'est à faire qu'une fois. La commande _trust_ permet que la connexion au périphérique se fasse automatiquement quand le périphérique est disponible : il ne faut pas lancer cette commande si on souhaite choisir le moment de la connexion. Pour se connecter, on utilise la commande _connect_. La commande _info_ permet de voir l'état du périphérique.
```
bluetoothctl:
  pair 88:C6:26:44:0C:EA
  trust 88:C6:26:44:0C:EA
  connect 88:C6:26:44:0C:EA
  info 88:C6:26:44:0C:EA
  quit
```
Pour déconnecter le périphérique,
```
bluetoothctl:
  disconnect 88:C6:26:44:0C:EA
  quit
```

# VOIR AUSSI
A écrire
