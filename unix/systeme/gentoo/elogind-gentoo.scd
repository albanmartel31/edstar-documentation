elogind-gentoo(7e)

# NAME
elogind-gentoo - Installer et configurer elogind pour gérer les dependances à systemd sous un système gentoo utilisant openrc

# INSTALLATION
## Le noyau
Configurer le noyau de la façon suivante :
```
General setup  --->
  [*] Control Group support  --->
File systems  --->
  [*] Inotify support for userspace
Power management and ACPI options --->
  [*] Suspend to RAM and standby
  [*] Hibernation (aka 'suspend to disk')
```

## Emerge
On utilise un USE flag global :
## Le noyau
```
root#  nano /etc/portage/make.conf
nano:
  USE="elogind -systemd"
root#  emerge -av --deep --changed-use @world
```

# CONFIGURATION
## Le service
```
root#  rc-update add elogind boot
root#  reboot
```

## Lancement d'une session au _startx_
```
lafrier$  nano ~/.xinitrc
nano:
  exec dbus-launch --exit-with-session openbox-session
```

## Execution d'un script lors de la mise en sommeil
Si on souhaite lancer un script lors des actions de suspension, d'hibernation, de réveil, etc, il faut placer le script dans le répertoire _/lib64/elogind/system-sleep/_ et utiliser les variables _$1_ et _$2_ en sachant que lors de l'appel du script elles prendont les valeurs suivantes :

	- _$1_ prend la valeur _pre_ ou la valeur _post_
	- _$2_ prend la valeur _suspend_ ou la valeur _hibernate_ ou la valeur _hybrid-sleep_

Voici un exemple de script (ne pas oublier de le rendre exécutable) :
```
root#  cd /lib64/elogind/system-sleep
root#  nano toto.bash
nano:
  #!/bin/bash
  case $1/$2 in
    pre/*)
      # Put here any commands expected to be run when suspending or hibernating.
      ;;
    post/*)
      # Put here any commands expected to be run when resuming from suspension or thawing from hibernation.
      ;;
  esac
root#  chmod +x toto.bash
```

# UTILISATION
Pour lancer une suspension :
```
lafrier$  loginctl suspend
```

RQ: _loginctl hibernate_ plante ma machine ...

# VOIR AUSSI
*elogind*(8)
