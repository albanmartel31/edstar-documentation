tmate-gentoo(7e)

# NAME
tmate-gentoo - Installer tmate sur gentoo

# INSTALLATION
```
root#  echo "app-misc/tmate ~amd64" >> /etc/portage/package.accept_keywords/tmate
root#  emerge -va app-misc/tmate
```

# VOIR AUSSI
*tmate*(1e)
