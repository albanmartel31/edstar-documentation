crossdev-gentoo(7e)

# NAME
crossdev-gentoo - Compilation croisée sur la distribution Gentoo

# DESCRIPTION

Cette documentation décrit l'installation et la configuration d'un
environnement de compilation croisée sous la distribution GNU/Linux Gentoo via
le paquet _sys-devel/crossdev_. Par compilation croisée, on entend la
procédure qui consiste à compiler un programme à destination d'une machine
dont l'architecture diffère de celle sur laquelle le programme est compilé.
Par exemple, compiler sur un ordinateur avec un processeur x86-64
(_amd64_) un programme à destination d'une machine utilisant
un processeur arm (_arm64_).

# INSTALLATION

```
root ~ #  emerge -av sys-devel/crossdev
```

L'utilitaire _crossdev_ place les paquets qu'il génère dans un _overlay_. On
peut au choix définir cet overlay via l'option de ligne de commande
_--ov-output_, définir un overlay global à toutes les cibles nommé _crossdev_,
ou en créer un par cible nommé _cross-<TARGET>_, avec _TARGET_ le nom de la
cible (cf ci-après pour déterminer la valeur de _TARGET_). Si aucune de ces
options n'est utilisée _crossdev_ placera ses paquets dans l'overlay avec la
plus faible priorité. Pour éviter de compromettre les _overlays_ existants, on
choisit ici de créer l'overlay _crossdev_ dans lequel l'utilitaire _crossdev_
viendra stocker les paquets de l'ensemble des chaînes de compilation qu'il
gère :

```
root ~ #  mkdir -p /var/db/repos/crossdev/{profiles,metadata}
root ~ #  echo "crossdev" >> /var/db/repos/crossdev/profiles/repo_name
root ~ #  nano /var/db/repos/crossdev/metadata/layout.conf
nano:
  masters = gentoo
  thin-manifests = true
root ~ #  nano /etc/portage/repos.conf/crossdev.conf
nano:
  [crossdev]
  location = /var/db/repos/crossdev
  priority = 10
  masters = gentoo
  auto-sync = no
```

## Installer la chaîne de génération pour _arm64_

Dans cette section on utilise _crossdev_ pour installer une chaîne de
génération pour architecture _arm64_. Pour ce faire, on commence par identifer
précisément la chaîne de génération que l'on souhaite utilisée sur la machine
cible. Pour déterminer le nom de cette chaîne de compilation on s'aidera du
résultat de la commande ci-dessous :

```
lafrier ~ $  crossdev --target help
```

Néanmoins, si un système Gentoo est déjà installé sur la machine cible,
d'aucun peut directement retrouver le nom de la chaîne de compilation utilisée
à l'aide de la commande *eselect*(1) :

```
root@pine64-pinephone / #  eselect binutils list
[1]  aarch64-unknown-linux-gnu-2.36.1 *
```

Une fois déterminé le nom de la chaîne de compilation, on peut dès lors
l'installer sur la machine hôte utilisée pour la compilation croisée :

```
root ~ #  crossdev -h
root ~ #  crossdev help
root ~ #  crossdev --stable --target aarch64-unknown-linux-gnu
```

Cette procédure va installer l'ensemble des outils nécessaires à la
compilation croisée dont notamment le compilateur. L'installation peut donc
prendre un certain temps.

On édite alors le fichier *make.conf*(5) de la chaîne de compilation
nouvellement installée pour renseigner *portage*(5) (le gestionnaire de paquet
de Gentoo) sur comment compiler les paquets qu'elle requiert. Par exemple, si
la machine cible est un Pinephone, un fichier de congiguration possible
serait le suivant :

```
root ~ #  nano /usr/aarch64-unknown-linux-gnu/etc/portage/make.conf
nano:
  # Note: profile variables are set/overridden in profile/ files:
  # etc/portage/profile/use.force (overrides kernel_* USE variables)
  # etc/portage/profile/make.defaults (overrides ARCH, KERNEL, ELIBC variables)
  CHOST=aarch64-unknown-linux-gnu
  CBUILD=x86_64-pc-linux-gnu

  HOSTCC=${CBUILD}-gcc

  ROOT=/usr/${CHOST}/

  CPU_FLAGS_ARM="edsp neon thumb vfp vfpv3 vfpv4 vfp-d32 aes sha1 sha2 crc32 v4 v5 v6 v7 v8 thumb2"
  USE="${ARCH} -pam -bindist -pulseaudio -jumbo-build alsa elogind X gles2 gles2-only"

  CFLAGS="-O2 -pipe -fomit-frame-pointer -march=armv8-a -mtune=cortex-a53"
  CXXFLAGS="${CFLAGS}"

  FEATURES="-collision-protect sandbox buildpkg noman noinfo nodoc"
  # Be sure we dont overwrite pkgs from another repo..
  PKGDIR=${ROOT}packages/
  PORTAGE_TMPDIR=${ROOT}tmp/

  PKG_CONFIG_PATH="${ROOT}usr/lib/pkgconfig/"
  #PORTDIR_OVERLAY="/usr/portage/local/"
```

On définit enfin le profil de *portage*(5) à utiliser par la chaîne de
compilation croisée :

```
root ~ #  mv /usr/aarch64-unknown-linux-gnu/etc/portage/make.profile /tmp
root ~ #  ln -s /var/db/repos/gentoo/profiles/default/linux/arm64/17.0 \
    /usr/aarch64-unknown-linux-gnu/etc/portage/make.profile
```

Si *distcc*(1) est utilisé pour invoquer la compilation croisée d'un programme
directemement depuis la machine cible (voir *distcc-gentoo*(7e)),
l'installation peut alors être considérée comme terminée. Il est entre autre
inutile de poursuivre l'installation d'un système cible sur la machine hôte
puisque la machine cible dispose déjà de son propre système.

## llvm

```
root ~ #  nano /etc/portage/package.use/llvm
nano:
  sys-devel/llvm llvm_targets_AArch64
  sys-devel/clang llvm_targets_AArch64
  dev-lang/rust llvm_targets_AArch64
root ~ #  emerge -av sys-devel/clang
root ~ #  ln -s /usr/bin/distcc /usr/lib/distcc/bin/rustc
root ~ #  ln -s /usr/bin/distcc /usr/lib/distcc/bin/aarch64-unknown-linux-gnu-clang
root ~ #  ln -s /usr/bin/distcc /usr/lib/distcc/bin/aarch64-unknown-linux-gnu-clang++
```

## Appliquer un correctif utilisateur

Définir le correctif :

```
root ~ #  mkdir -p /usr/aarch64-unknown-linux-gnu/etc/portage/patches/sys-devel/gcc-11.2.0
root ~ #  cp correctif.path /usr/aarch64-unknown-linux-gnu/etc/portage/patches/sys-devel/gcc-11.2.0
```

## LLVM ET RUST

```
root ~ #  echo "sys-devel/llvm llvm_targets_AArch64" >> /etc/portage/package.use/llvm
root ~ #  echo "sys-devel/rust llvm_targets_AArch64" >> /etc/portage/package.use/rust
root ~ #  echo "sys-util/rustup ~amd64" >> /etc/portage/package.accept_keywords/rustup
root ~ #  emerge -av sys-devel/llvm
root ~ #  emerge -av sys-devel/rust
root ~ #  emerge -av sys-devel/rustup
root ~ #  rustup target add aarch64-unknown-linux-gnu
```

## Installer les bases du système _arm64_ (aka le _stage3_)

On va ici installer le _stage3_ de l'architecture cible. Pour ce faire, on
peut simplement suivre la procédure employé lors d'une installation de Gentoo
classique et télécharger un _stage3_ précompilé à décompresser dans le
répertoire _/usr/aarch64-unknown-linux-gnu/_ (voir *amd64-gentoo*(7e)). On
pourra alors passer directement à la section suivante.

Dans la suite on va se préter à l'exercice de générer les bases du système
cible à partir de rien. On encourage cependant le lecteur à télécharger un
_stage3_ existant ; sa génération étant assez laborieuse.

On débute par installer les paquets systèmes. L'activation ou la désaction de
"use flags" par paquet ne sert ici qu'à répondre aux attendus dy système de
paquet tel qu'ils les formules _à cette étape_ et fonction du système hôte
utilisé pour les déployer. D'aucun pourra modifier ces options une fois que
l'on aura basculer dans l'environnement natif via *chroot*(1). À noter qu'ici
il a été nécessaire de masquer certains paquets pour assurer la réussite de la
procédure :

```
root ~ #  ln -s /var/db/repos/gentoo/profiles/default/linux/arm64/17.0 \
    /usr/aarch64-unknown-linux-gnu/etc/portage/make.profile
root ~ #  mkdir -p /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/
root ~ #  mkdir -p /usr/aarch64-unknown-linux-gnu/etc/portage/package.unmask/
root ~ #  mkdir -p /usr/aarch64-unknown-linux-gnu/etc/portage/package.mask/
root ~ #  mkdir -p /usr/aarch64-unknown-linux-gnu/etc/portage/package.accept_keywords/
root ~ #  echo "=sys-libs/ncurses-6.2_p20210619" \
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.mask/ncurses \
root ~ #  echo "=virtual/libcrypt-2" \
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.mask/libcrypt
root ~ #  echo ">sys-devel/gcc-10.3.0-r2"
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.mask/gcc
root ~ #  echo "net-misc/rsync acl iconv ipv6 ssl xattr"
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/rsync
root ~ #  echo "sys-libs/ncurses unicode"
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/ncurses
root ~ #  echo "virtual/libcrypt-2 prefix-guest -elibc-glibc" \
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/libcrypt-2
root ~ #  aarch64-unknown-linux-gnu-emerge -uva --keep-going @system
```

On installe ensuite les paquets essentiels pour générer le _stage1_ du
système cible :

```
root ~ #  echo "*/* python_targets_python3_9" \
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/python3.9
root ~ #  echo "app-crypt/gnupg ssl" \
    >> /usr/aarch64-unknown-linux-gnu/etc/portage/package.use/gnupg
root ~ # aarch64-unknown-linux-gnu-emerge -uva1 --keep-going \
    $(egrep '^[a-z]+' /var/db/repos/gentoo/profiles/default/linux/packages.build)
```

À noter que la compilation des paquets précédents peut échouer ; c'est le cas
des paquets dont la compilation doit forcément s'opérer au sein de
l'environnement cible. Leur installation sera corrigée une fois que l'on aura
basculé dans ce nouvel environnementcible. Attention cependant ! À la fin de
cette étape, d'aucun doit bien disposer des paquets essentiels au système de
base que l'on cherche à installer tels que _sys-apps/portage_,
_sys-apps/shadow_, _sys-devel/gcc_ ou encore _app-shells/bash_.

## Émuler l'architecture _arm64_

Si l'environnement cible utilise une architecture différente de l'architecture
hôte, on doit au préalable installer *qemu*(1) que l'on utilisera pour émuler
le processeur du système cible. Pour cela on commence par s'assurer que le
noyau courant dispose des fonctionnalités requises par *qemu*(1). Dans le cas
contraire on génère et installe un noyau configuré en conséquence :

```
root ~ #  cd /usr/src/linux
root /usr/src/linx # make menuconfig
  CONFIG_KVM=y
  CONFIG_KVM_INTEL=y # Si processeur Intel
  CONFIG_TUN=y
root /usr/src/linux #  make && make modules
root /usr/src/linux #  make install && make modules_install
root /usr/src/linux #  grub-mkconfig -o /boot/grub/grub.cfg
root /usr/src/linux #  reboot
```

On peut alors installer *qemu*(1) pour l'architecture cible à savoir dans
notre cas l'architecture _arm64_ :

```
root ~ #  emerge -av app-emulation/qemu
root ~ #  QEMU_USER_TARGETS="aarch64" QEMU_SOFTMMU_TARGETS="aarch64" \
    USE="static-user static-libs" emerge -av --buildpkg --oneshot qemu
root ~ #  cd /usr/aarch64-unknown-linux-gnu && ROOT=$PWD/ \
    emerge -av --usepkgonly --oneshot --nodeps qemu
```

On peut alors tester l'état de l'installation courante avec un premier
changement d'environnement :

```
root ~ #  /etc/init.d/qemu-binfmt start
root ~ #  chroot /usr/aarch64-unknown-linux-gnu /bin/bash --login
root / #  exit
```

## Basculer sur le système _arm64_

On va désormais écrire un script Bash pour automatiser la changement
d'environnement vers le système cible. Ce script assure que la couche
d'émulation est bien active, monte les répertoires requis dans le système
cible avant de basculer dans cet environnement :

```
root ~ #  cp /usr/aarch64-unknown-linux-gnu/etc/make.conf \
 /usr/aarch64-unknown-linux-gnu/etc/crossdev-make.conf \
root ~ #  cp /usr/aarch64-unknown-linux-gnu/etc/make.conf \
 /usr/aarch64-unknown-linux-gnu/etc/chroot-make.conf \
root ~ #  nano /usr/aarch64-unknown-linux-gnu/etc/chroot-make.conf
nano:
  COMMON_FLAGS="-O2 -pipe -march=armv8-a -mtune=cortex-a53"
  CFLAGS="${COMMON_FLAGS}"
  CXXFLAGS="${COMMON_FLAGS}"
  FCFLAGS="${COMMON_FLAGS}"
  FFLAGS="${COMMON_FLAGS}"

  MAKEOPTS="-j8"

  FEATURES="-pid-sandbox"

  # WARNING: Changing your CHOST is not something that should be done lightly.
  # Please consult https://wiki.gentoo.org/wiki/Changing_the_CHOST_variable before changing.
  CHOST="aarch64-unknown-linux-gnu"

  CPU_FLAGS_ARM="edsp neon thumb vfp vfpv3 vfpv4 vfp-d32 aes sha1 sha2 crc32 v4 v5 v6 v7 v8 thumb2"
  USE="-bindist -pulseaudio -jumbo-build alsa elogind X gles2 gles2-only"

  VIDEO_CARDS="lima"

  LC_MESSAGES=C
```

```
root ~ #  touch /usr/local/bin/chroot-aarch
root ~ #  chmod 755 /usr/local/bin/chroot-aarch64
root ~ #  nano /usr/local/bin/chroot-aarch64
nano:
  #!/bin/bash
  cd /usr/aarch64-unknown-linux-gnu

  # La ligne suivante est nécessaire uniquement si le processeur cible doit
  # être émulé (cf section précédente)
  ./etc/init.d/qemu-binfmt start

  mount -t proc none ./proc
  mount -o bind /dev ./dev
  mount -o bind /lib/modules ./lib/modules
  mount -o bind /var/db/repos/gentoo ./var/db/repos/gentoo
  mount -o bind /var/db/repos/crossdev ./var/db/repos/crossdev
  mount -o bind /var/db/repos/bingch ./var/db/repos/bingch
  mount -o bind /sys ./sys
  mount -o bind /tmp ./tmp
  cp /etc/resolv.conf ./etc/resolv.conf

  rm -f /usr/aarch64-unknown-linux-gnu/etc/portage/make.conf
  cd /usr/aarch64-unknown-linux-gnu/etc/portage/
  ln -s ./chroot-make.conf make.conf
  cd -

  chroot . /bin/bash --login

  rm -f /usr/aarch64-unknown-linux-gnu/etc/portage/make.conf
  ln -s /usr/aarch64-unknown-linux-gnu/etc/portage/crossdev-make.conf \
    /usr/aarch64-unknown-linux-gnu/etc/portage/make.conf

  umount ./tmp
  umount ./sys
  umount ./lib/modules
  umount ./var/db/repos/crossdev
  umount ./var/db/repos/gentoo
  umount ./dev
  umount ./proc

  ./etc/init.d/qemu-binfmt stop
```

On peut alors utiliser ce script pour basculer dans le nouvel environnement :

```
root ~ #  /usr/local/bin/chroot-aarch64
root / #  gcc-config -l; ldconfig -v; ROOT=/ env-update
root / #  source /etc/profile
root / #  export TERM=xterm-256color
root / #  export PS1="(chroot aarch64) $PS1"
```

## Installer le système

Une fois dans le nouvel environement on commence par modifier le fichier
*make.conf*(5) comme si nous étions sur la machine cible. On supprime entre
autre les options utilisées pour la compilation croisée car une fois dans le
nouvel environnement, toute compilation est de fait une compilation croisée
puisque s'opérant au sein de celui ci.

```
(chroot aarch64) root / #  nano /etc/portage/make.conf
nano:
  COMMON_FLAGS="-O2 -pipe -march=armv8-a -mtune=cortex-a53"
  CFLAGS="${COMMON_FLAGS}"
  CXXFLAGS="${COMMON_FLAGS}"
  FCFLAGS="${COMMON_FLAGS}"
  FFLAGS="${COMMON_FLAGS}"

  MAKEOPTS="-j8"

  FEATURES="-pid-sandbox"

  # WARNING: Changing your CHOST is not something that should be done lightly.
  # Please consult https://wiki.gentoo.org/wiki/Changing_the_CHOST_variable before changing.
  CHOST="aarch64-unknown-linux-gnu"

  CPU_FLAGS_ARM="edsp neon thumb vfp vfpv3 vfpv4 vfp-d32 aes sha1 sha2 crc32 v4"
  USE="-bindist -pulseaudio -jumbo-build alsa elogind X gles2 gles2-only"

  VIDEO_CARDS="lima"

  LC_MESSAGES=C
```

À noter la ligne _FEATURES="-pid-sandbox"_ nécessaire pour l'architecture
_arm64_ émulée par *qemu*(1). En l'absence de cette option, installer un
paquet afficherait l'erreur : _qemu: qemu_thread_create: Invalid argument_.

On peut poursuivre alors l'installation de manière classique (voir
*amd64-gentoo*(7e)) :

```
(chroot aarch64) root / #  nano /etc/local/gen
nano:
  en_US.UTF-8 UTF-8
  fr_FR.UTF-8 UTF-8
(chroot aarch64) root / #  locale-gen
(chroot aarch64) root / #  eselect locale list
(chroot aarch64) root / #  eselect locale set 3
(chroot aarch64) root / #  emerge --sync # Optionnel
(chroot aarch64) root / #  eselect news list # Optionnel
(chroot aarch64) root / #  eselect news read # Optionnel
(chroot aarch64) root / #  eselect profile list
[1]   default/linux/aarch64/17.0 (stable) *
...
(chroot aarch64) root / #  eselect profile set 1
```

# VOIR AUSSI

- [1] <https://wiki.gentoo.org/wiki/Cross_build_environment>
- [2] <https://wiki.gentoo.org/wiki/Custom_ebuild_repository#Crossdev>
- [3] <https://wiki.gentoo.org/wiki/Distcc/Cross-Compiling>

*distcc-gentoo*(7e), *make.conf*(5), *portage*(5), *qemu*(1)
