gpg(1e)

# NAME
gpg - utilisation de gpg

# DESCRIPTION
protéger un fichier par chiffrement 

# CONFIGURATION
Ouvrez une fenêtre d’invite de commande et générez une clé GPG :

	gpg --gen-key	

Configurer l’agent de cache de mot de passe

	dans le fichier : ~/.gnupg/gpg-agent.conf 

	copier la ligne : default-cache-ttl 1 max-cache-ttl 1

	redémarrez l’agent avec la commande : echo RELOADAGENT | gpg-connect-agent        


# UTILISATION
Chiffrement du fichier toto.txt (qui genere un fichier "toto.txt.gpg")

	gpg -c toto.txt

Dechiffrer le fichier "toto.txt.gpg" (sur la console)

	gpg -d toto.txt.gpg

Dechiffrer le fichier "toto.txt.gpg" (dans un fichier "tata")

	gpg -o tata -d toto.txt.gpg
