shellcheck(1e)

# NAME
shellcheck - Permer de controller la synthaxe d'un script shell

# DESCRIPTION
*shellcheck* est un outil qui permet de diagnostiquer un script shell. Il nous
permet notamment de vérifier qu'il est compatible avec le standard POSIX
si le script shell en question mentionne dans son en-tête l'utilisation de
l'interpréteur de commande *sh*(1p).

# UTILISATION
Pour verifier la synthaxe du script shell _toto.sh_ :
```
lafrier$  shellcheck -x -o all toto.sh
```

# VOIR AUSSI
*sh*(1p)

