sshfs(1e)

# NAME
sshfs - utilisation de sshfs

# DESCRIPTION
*sshfs*(1e) permet de monter sur son système de fichier, un autre système de fichier distant, à travers une connexion SSH.

# UTILISATION
Creer un repertoire pour le montage _disque_montage_ :

	mkdir disque_montage

Monter le repertoire distant (par exemple sur nastar defini dans le config de ssh) sur _disque_montage_

	sshfs nastar:/nastar/lafrier disque_montage

Demonter aprés avoir vérifier que le point de montage n'est plus sollicité (par exemple un fichier ouvert)

	fusermount -u _disque_montage_

