html(5e)

# NAME
html - le langage

# DESCRIPTION
*html*(5e) est un langage de composition de documents lisibles par un navigateur
web.

# NIVEAUX D'ECRITURE
Les niveaux de titre :
```
<h1>Titre de niveau 1</h1>
<h2>Titre de niveau 2</h2>
<h3>Titre de niveau 3</h3>
<h4>Titre de niveau 4</h4>
<h5>Titre de niveau 5</h5>
<h6>Titre de niveau 6</h6>
```

Le texte principal (pour chaque paragraphe) :
```
<p>Du texte ...</p>
```
Les items avec point :
```
<ul>
  <li>Premier item</li>
  <li>Second item</li>
</ul>
```
Les items sans point :
```
<ul style="list-style-type: none">
  <li>Premier item</li>
  <li>Second item</li>
</ul>
```

# CARACTERES SPECIAUX
Blanc inseccable :
```
&#8239;
```
Exemple d'utilisation du blanc inseccable pour écrire un : et un ; en français :
```
En français, les signes de ponctuation doubles sont précédés d'un blanc
inseccable&#8239;: on doit donc le faire systématiquement&#8239;; c'est une
convention.
```
Le \& commercial :
```
&amp;
```
